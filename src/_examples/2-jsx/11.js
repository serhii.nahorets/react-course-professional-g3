/* Core */
import { render } from 'react-dom';

const isPizzaOpen = false;
const welcome = <h1>Welcome to Super Pizza!</h1>;
const goodbye = <h1>See you tomorrow!</h1>;

const element2 = isPizzaOpen && welcome;

render(
    <>
        {isPizzaOpen ? welcome : goodbye} {element2}
    </>,
    document.getElementById('root'),
);
